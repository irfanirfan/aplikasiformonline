export class Menu {
    placeholder: string;
    name: string;
    type: string;
    formType:string;
    canBeShared:boolean;
    oneFillOnly:boolean;
}