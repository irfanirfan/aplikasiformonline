import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router';
import { Profile } from './models/profile';
import { AnalyticsharedService } from './services/analyticshared.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  userProfile: Profile;
  primaryColor: string;
  theme
  constructor(public authService: AuthService, private router: Router, private analytics: AnalyticsharedService) {
    this.analytics.logEvents("User_Come")
  }

  ngOnInit(): void {
    this.authService.afAuth.authState.subscribe(result => {
      this.userProfile = this.authService.getUserProfile()
    })
  }

  savePrimaryColor(color) {
    this.primaryColor = color
  }


  onLogout() {
    this.authService.logout()
    localStorage.clear()
    setTimeout(async () => {
      this.router.navigate(['login']).then(() => {
        window.location.reload()
      });
    }, 400);

  }
}


