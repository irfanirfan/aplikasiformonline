import { Injectable } from '@angular/core';
import { AngularFireAnalytics } from '@angular/fire/analytics';
import * as firebase from 'firebase/app';
import 'firebase/analytics';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsharedService {
  analytics: firebase.default.analytics.Analytics;
  constructor() {
    this.analytics = firebase.default.analytics();
  }

  logEvents(eventName: any): void {
    // shared method to log the events
    this.analytics.logEvent(eventName);
  }
}
