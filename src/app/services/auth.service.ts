import { Injectable, DebugElement } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Profile } from '../models/profile';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PopupService } from './popup.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth, private snackbar: MatSnackBar) {
    this.afAuth.authState.subscribe(user => {
      if (user == null)
        return
      localStorage.setItem('uid', user.uid)
      localStorage.setItem('email', user.email)
    })
  }

  getUserProfile() {
    let user: any = JSON.parse(localStorage.getItem('userData'))
    if (user == null) return
    let userData = new Profile
    userData.key = user.key
    userData.email = user.email
    userData.theme = user.theme
    userData.level = user.level
    return userData
  }

  isLoggedIn() {
    let user = localStorage.getItem('uid')
    if (user == null)
      return false
    return true
  }

  login(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
  }

  register(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
  }

  remove(email: string, password: string) {
    this.afAuth.signInWithEmailAndPassword(email, password)
      .then(async (info) => {
        var user = await this.afAuth.currentUser;
        user.delete()
      });
  }

  logout() {
    localStorage.clear()
    return this.afAuth.signOut();
  }

  snackbarActive(message: string, duration: number) {
    return this.snackbar.open(message, "OK", {
      duration: duration,
      verticalPosition: 'top'
    })
  }
}
