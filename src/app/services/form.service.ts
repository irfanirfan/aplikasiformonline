import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FormService {


  form: AngularFireList<any>;
  constructor(private db: AngularFireDatabase) { }

  get(userId: string, type: string) {
    return this.db.list(`forms/${type}/${userId}`).snapshotChanges()
  }

  getLimit5() {
    return this.db.list(`forms`, ref => ref.limitToFirst(5)).snapshotChanges().pipe(
      map((changes: any) =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getAll(type: string) {
    return this.db.list(`forms/${type}`).snapshotChanges()
  }

  getById(userId: string, type: string, id: string) {
    return this.db.object(`forms/${type}/${userId}/${id}`).valueChanges()
  }

  getTemplate(type: string) {
    return new Promise((resolve, reject) => {
      this.db.object(`forms/${type}/template`).valueChanges().subscribe(
        (resp: any) => { resolve(resp) },
        (err: any) => { reject(err.error.message) }
      )
    });
  }

  getSharedList(type: string): Observable<any> {
    return this.db.object(`forms/${type}/sharedId`).valueChanges()
  }

  getForms(type: string) {
    return new Promise((resolve, reject) => {
      this.db.object(`forms/${type}`).valueChanges().subscribe(
        (resp: any) => { resolve(resp) },
        (err: any) => { reject(err.error.message) }
      )
    });
  }

  insert(data: any, param: any = {}) {
    return this.db.list(`forms/${param.type}/${param.formId}`).push(data)
  }

  insertTemplate(type: string, data: any) {
    return this.db.object(`forms/${type}/template`).set(data)
  }

  insertUpdatedId(type:string, data:any){
    return this.db.object(`forms/${type}`).set(data)
  }

  insertSharedId(type: string, data: any) {
    return this.db.object(`forms/${type}/sharedId`).set(data)
  }

  update(data: any, param: any = {}) {
    return this.db.object(`forms/${param.type}/${param.formId}/${param.dataId}`).update(data)
  }

  delete(data: any) {
    return this.db.object(`forms/${data.type}/${data.formId}/${data.id}`).remove()
  }

  deleteForm(type:any){
    return this.db.object(`forms/${type}`).remove()
  }

  deleteSharedId(type: any) {
    return this.db.object(`forms/${type}/sharedId`).remove()
  }
}
