import { TestBed } from '@angular/core/testing';

import { AnalyticsharedService } from './analyticshared.service';

describe('AnalyticsharedService', () => {
  let service: AnalyticsharedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AnalyticsharedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
