import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { Menu } from '../models/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  menu: AngularFireList<any>;



  constructor(private db: AngularFireDatabase) { }

  add(key: string, newMenu: Menu) {
    return this.db.list(`/menu/${key}`).push(newMenu)
  };

  get(key: string) {
    return this.db.list(`menu/${key}`).snapshotChanges()
  }

  getById(key: string, menuKey: string) {
    return this.db.object(`menu/${key}/${menuKey}`).valueChanges()
  }

  getLimit5(key: string) {
    return this.db.list(`menu/${key}`, ref => ref.limitToFirst(5)).snapshotChanges().pipe(
      map((changes: any) =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  getByType(key: string, type: string) {
    return this.db.list(`menu/${key}`, ref => ref.orderByChild('type').equalTo(type)).snapshotChanges()
  }

  remove(key: string, menu: any) {
    return this.db.list(`menu/${key}/${menu}`).remove()
  }

  update(key: string, menuKey: string, newMenu: Menu) {
    return this.db.object(`/menu/${key}/${menuKey}`).update(newMenu)
  };



}
