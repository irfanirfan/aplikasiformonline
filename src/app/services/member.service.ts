import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';
import { map } from 'rxjs/operators';
import { Profile } from '../models/profile';

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  constructor(private db: AngularFireDatabase) { }

  add(key: string, newMember: Profile) {
    return this.db.list(`users/${key}`).push(newMember)
  };

  get(key: string) {
    return this.db.list(`users/${key}`).snapshotChanges()
  }

  getAll() {
    return this.db.list(`users`, ref => ref.orderByChild('level').equalTo('member')).snapshotChanges()
  }

  getLimit5() {
    return this.db.list(`users`, ref => ref.limitToFirst(5)).snapshotChanges().pipe(
      map((changes: any) =>
        changes.map(c => ({ key: c.payload.key, ...c.payload.val() }))
      )
    );
  }

  remove(key: string) {
    return this.db.list(`users/${key}`).remove()
  }

}
