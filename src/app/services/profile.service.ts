import { Injectable } from '@angular/core';
import { Profile } from '../models/profile';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  profile: AngularFireObject<Profile>
  constructor(private db: AngularFireDatabase) { }

  update(profile: Profile) {
    return this.db.object('users/' + profile.key).update(profile)
  }

  set(profile: Profile) {
    return this.db.object('users/' + profile.key).set(profile)
  }

  getbyId(userId: string): Observable<any> {
    return this.db.object('users/' + userId).valueChanges()
  }

  get() {
    throw new Error("Method not implemented.");
  }
}
