import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from '../shared/modal/dialog/dialog.component';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor(private dialog: MatDialog) { }

  openDialog(param) {
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: param
    })
  }

  successDialog(param) {
    param.title = "Success"
    param.showNoButton = false
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: param
    })
  }

  errorDialog(param) {
    param.title = "Error"
    param.showNoButton = false
    return this.dialog.open(DialogComponent, {
      width: '350px',
      data: param
    })
  }


}
