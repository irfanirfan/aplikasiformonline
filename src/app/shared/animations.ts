import {
    transition,
    trigger,
    query,
    style,
    animate,
    group,
    animateChild,
    sequence,
    stagger
} from '@angular/animations';

export const ROUTE_ANIMATIONS_ELEMENTS = 'route-animations-elements';

export const fadeAnimation = trigger("fadeAnimation", [
    transition("* <=> *", [
        query(':enter > *', style({ opacity: 0, position: 'fixed' }), {
            optional: true
        }),
        sequence([
            query(
                ':leave > *',
                [
                    style({ transform: 'translateY(0%)', opacity: 1 }),
                    animate(
                        '0.2s ease-in-out',
                        style({ transform: 'translateY(-3%)', opacity: 0 })
                    ),
                    style({ position: 'fixed' })
                ],
                { optional: true }
            ),
            query(
                ':enter > *',
                [
                    style({
                        transform: 'translateY(-3%)',
                        opacity: 0,
                        position: 'static'
                    }),
                    animate(
                        '0.5s ease-in-out',
                        style({ transform: 'translateY(0%)', opacity: 1 })
                    )
                ],
                { optional: true }
            )
        ]),
    ])
]);


const STEPS_ALL: any[] = [
    query(':enter > *', style({ opacity: 0, position: 'fixed' }), {
        optional: true
    }),
    sequence([
        query(
            ':leave > *',
            [
                style({ transform: 'translateY(0%)', opacity: 1 }),
                animate(
                    '0.2s ease-in-out',
                    style({ transform: 'translateY(-3%)', opacity: 0 })
                ),
                style({ position: 'fixed' })
            ],
            { optional: true }
        ),
        query(
            ':enter > *',
            [
                style({
                    transform: 'translateY(-3%)',
                    opacity: 0,
                    position: 'static'
                }),
                animate(
                    '0.5s ease-in-out',
                    style({ transform: 'translateY(0%)', opacity: 1 })
                )
            ],
            { optional: true }
        )
    ]),
];

// const STEPS_PAGE = [STEPS_ALL[0], STEPS_ALL[2]];
export const routeAnimations = trigger('routeAnimations', [
    transition('* <=> *', STEPS_ALL),
]);