import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { ProfileService } from 'src/app/services/profile.service';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent {
  fb: FormGroup
  constructor(public dialogRef: MatDialogRef<SignUpComponent>, private authService: AuthService, private profileService: ProfileService,
    @Inject(MAT_DIALOG_DATA) public data: any, private snackbar: MatSnackBar) {
    this.fb = new FormGroup({
      name: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });

  }

  onNoClick() {
    this.dialogRef.close()
  }

  onSubmit() {
    if (this.fb.valid) {
      this.authService.register(this.fb.value.email, this.fb.value.password).then(data => {
        this.snackbar.open('Successfully Registered, You can now Login', "X", {
          duration: 1500,
          verticalPosition: 'top'
        })
        this.dialogRef.close()
        let profile = new Profile
        this.fb.value.key = data.user.uid
        profile = this.fb.value
        profile.level = "member"
        this.profileService.set(profile)
      }).catch(error => {
        if (error) {
          console.log(error)
          this.snackbar.open(error, "X", {
            duration: 2500,
            verticalPosition: 'top'
          })
        }
      })
    } else {
      this.snackbar.open('Your Email or Password Is Invalid', "X", {
        duration: 2500,
        verticalPosition: 'top'
      })
    }
  }

}
