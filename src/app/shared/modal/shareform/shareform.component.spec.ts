import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ShareformComponent } from './shareform.component';

describe('ShareformComponent', () => {
  let component: ShareformComponent;
  let fixture: ComponentFixture<ShareformComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ShareformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
