import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignUpComponent } from '../sign-up/sign-up.component';
import { AuthService } from 'src/app/services/auth.service';
import { ProfileService } from 'src/app/services/profile.service';
import { MemberService } from 'src/app/services/member.service';

@Component({
  selector: 'app-shareform',
  templateUrl: './shareform.component.html',
  styleUrls: ['./shareform.component.scss']
})
export class ShareformComponent implements OnInit {
  fb: FormGroup
  members: any;
  // members: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  constructor(public dialogRef: MatDialogRef<SignUpComponent>, private authService: AuthService, private profileService: ProfileService,
    @Inject(MAT_DIALOG_DATA) public data: any, private snackbar: MatSnackBar, private memberService: MemberService) {
    this.fb = new FormGroup({
      user: new FormControl(null, Validators.required),
    });

  }
  ngOnInit() {
    this.memberService.getAll().subscribe(data => {
      this.members = []
      this.members = data.map(x => { return { key: x.key, data: x.payload.val() } })
    })
  }

  onNoClick() {
    this.dialogRef.close()
  }

  onSubmit() {
    this.dialogRef.close(this.fb.value.user)
  }

}
