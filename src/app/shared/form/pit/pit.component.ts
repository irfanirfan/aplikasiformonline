import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormService } from 'src/app/services/form.service';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pit',
  templateUrl: './pit.component.html',
  styleUrls: ['./pit.component.scss']
})
export class PITComponent implements OnInit {
  inputs = [
    {
      type: "text",
      name: "name",
      placeholder: "Nama"
    },
    {
      type: "text",
      name: "nomorkaryawan",
      placeholder: "Nomor Karyawan"
    },
    {
      type: "text",
      name: "jabatan",
      placeholder: "Jabatan"
    },
    {
      type: "date",
      name: "date",
      placeholder: "Hari/Tanggal"
    },
    {
      type: "date",
      name: "start",
      placeholder: "Mulai"
    },
    {
      type: "date",
      name: "end",
      placeholder: "Sampai"
    },
    {
      type: "text",
      name: "tujuan",
      placeholder: "Tujuan"
    },
  ]
  fb = new FormGroup({
    'name': new FormControl('', Validators.required),
    'nomorkaryawan': new FormControl('', Validators.required),
    'jabatan': new FormControl('', Validators.required),
    'date': new FormControl('', Validators.required),
    'start': new FormControl('', Validators.required),
    'end': new FormControl('', Validators.required),
    'tujuan': new FormControl('', Validators.required),
  })
  constructor(
    private formService:FormService,
    private authService:AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    if (this.fb.valid) {
      console.log(this.fb.value)
      const DATA = this.fb.value
      DATA.userId = this.authService.getUserProfile().key
      DATA.type = "PIT"
      this.formService.insert(DATA).then(() => {
        this.authService.snackbarActive("Berhasil Dibuat", 1500)
        this.router.navigate(['/dashboard/tabledata/' + DATA.type + '/' + DATA.userId])
      }).catch(error => {
        this.authService.snackbarActive("Gagal Dibuat, karena" + error, 200)
        console.log(error)
      })
    } else {
      this.authService.snackbarActive("Data tidak boleh ada yang kosong", 2000)
    }
  }

}
