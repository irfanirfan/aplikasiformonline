import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PITComponent } from './pit.component';

describe('PITComponent', () => {
  let component: PITComponent;
  let fixture: ComponentFixture<PITComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PITComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PITComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
