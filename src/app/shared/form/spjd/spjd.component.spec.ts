import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SPJDComponent } from './spjd.component';

describe('SPJDComponent', () => {
  let component: SPJDComponent;
  let fixture: ComponentFixture<SPJDComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SPJDComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SPJDComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
