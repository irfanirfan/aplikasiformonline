import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SIPKComponent } from './sipk.component';

describe('SIPKComponent', () => {
  let component: SIPKComponent;
  let fixture: ComponentFixture<SIPKComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SIPKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SIPKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
