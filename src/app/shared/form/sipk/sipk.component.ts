import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FormService } from 'src/app/services/form.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sipk',
  templateUrl: './sipk.component.html',
  styleUrls: ['./sipk.component.scss']
})
export class SIPKComponent implements OnInit {
  inputs = [
    {
      type: "number",
      name: "nopol",
      placeholder: "No. Polisi"
    },
    {
      type: "date",
      name: "date",
      placeholder: "Tanggal"
    },
    {
      type: "text",
      name: "pengemudi",
      placeholder: "Pengemudi"
    },
    {
      type: "number",
      name: "jumlahPenumpang",
      placeholder: "Jumlah Penumpang"
    },
    {
      type: "time",
      name: "start",
      placeholder: "Mulai jam"
    },
    {
      type: "time",
      name: "end",
      placeholder: "Selesai Jam"
    },
    {
      type: "text",
      name: "penumpang",
      placeholder: "Nama Penumpang"
    },
  ]
  fb = new FormGroup({
    'nopol': new FormControl('', Validators.required),
    'date': new FormControl('', Validators.required),
    'pengemudi': new FormControl('', Validators.required),
    'jumlahPenumpang': new FormControl('', Validators.required),
    'start': new FormControl('', Validators.required),
    'end': new FormControl('', Validators.required),
    'penumpang': new FormControl('', Validators.required),
  })
  constructor(
    private authService: AuthService,
    private formService: FormService,
    private router: Router
  ) { }

  ngOnInit() {
  }


  onSubmit() {
    if (this.fb.valid) {
      console.log(this.fb.value)
      const DATA = this.fb.value
      DATA.userId = this.authService.getUserProfile().key
      DATA.type = "SIPK"
      this.formService.insert(DATA).then(() => {
        this.authService.snackbarActive("Berhasil Dibuat", 1500)
        this.router.navigate(['/dashboard/tabledata/' + DATA.type + '/' + DATA.userId])
      }).catch(error => {
          this.authService.snackbarActive("Gagal Dibuat, karena" + error, 200)
          console.log(error)
        })
    } else {
      this.authService.snackbarActive("Data tidak boleh ada yang kosong", 2000)
    }
  }
}
