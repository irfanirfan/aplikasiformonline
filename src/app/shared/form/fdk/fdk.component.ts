import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AuthService } from 'src/app/services/auth.service';
import { FormService } from 'src/app/services/form.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-fdk',
  templateUrl: './fdk.component.html',
  styleUrls: ['./fdk.component.scss']
})
export class FDKComponent implements OnInit {
  inputs = [
    {
      type: "text",
      name: "name",
      placeholder: "Nama"
    },
    {
      type: "text",
      name: "jabatan",
      placeholder: "Jabatan"
    },
    {
      type: "text",
      name: "nik",
      placeholder: "NIK"
    },
    {
      type: "text",
      name: "assigned",
      placeholder: "Yang Menugaskan"
    },
    {
      type: "text",
      name: "kepdinas",
      placeholder: "Keperluan Dinas"
    },
    {
      type: "text",
      name: "tujuan",
      placeholder: "Tujuan"
    },
    {
      type: "date",
      name: "dateDeparture",
      placeholder: "Tanggal keberangkatan"
    },
    {
      type: "date",
      name: "dateArrival",
      placeholder: "Tanggal Rencana Kepulangan"
    },
  ]
  fb = new FormGroup({
    'name': new FormControl('', Validators.required),
    'jabatan': new FormControl('', Validators.required),
    'nik': new FormControl('', Validators.required),
    'assigned': new FormControl('', Validators.required),
    'kepdinas': new FormControl('', Validators.required),
    'tujuan': new FormControl('', Validators.required),
    'dateDeparture': new FormControl('', Validators.required),
    'dateArrival': new FormControl('', Validators.required),
  })

  constructor(
    private authService: AuthService,
    private formService: FormService,
    private router: Router) { }

  ngOnInit() {
   
  }

  onSubmit() {
    if (this.fb.valid) {
      console.log(this.fb.value)
      const DATA = this.fb.value
      DATA.userId = this.authService.getUserProfile().key
      DATA.type = "FDK"
      this.formService.insert(DATA).then(() => {
        this.authService.snackbarActive("Berhasil Dibuat", 1500)
        this.router.navigate(['/dashboard/tabledata/' + DATA.type + '/' + DATA.userId])
      }).catch(error => {
        this.authService.snackbarActive("Gagal Dibuat, karena" + error, 200)
        console.log(error)
      })
    } else {
      this.authService.snackbarActive("Data tidak boleh ada yang kosong", 2000)
    }
  }



}
