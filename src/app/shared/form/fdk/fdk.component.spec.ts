import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { FDKComponent } from './fdk.component';

describe('FDKComponent', () => {
  let component: FDKComponent;
  let fixture: ComponentFixture<FDKComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FDKComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FDKComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
