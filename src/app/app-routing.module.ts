import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AuthguardGuard } from './services/authguard.guard';
import { ProfileComponent } from './pages/profile/profile.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { AddFormComponent } from './pages/add-form/add-form.component';
import { ListComponent } from './pages/list/list.component';
import { MemberComponent } from './pages/member/member.component';


let routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard/homepage',
    pathMatch: "full"
  },
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: "full",
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthguardGuard],
  },
  {
    path: 'profile',
    canActivate: [AuthguardGuard],
    pathMatch: "full",
    component: ProfileComponent,
  },
  {
    path: 'createform',
    canActivate: [AuthguardGuard],
    pathMatch: "full",
    component: AddFormComponent
  },
  {
    path: 'list',
    loadChildren: () => import('./pages/list/list.module').then(m => m.ListModule),
    canActivate: [AuthguardGuard],
  },
  {
    path: 'member',
    canActivate: [AuthguardGuard],
    pathMatch: "full",
    component: MemberComponent
  },
  { path: '**', component: DashboardComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
