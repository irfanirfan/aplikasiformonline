import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MenuService } from 'src/app/services/menu.service';
import { AuthService } from 'src/app/services/auth.service';
import { Menu } from 'src/app/models/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormService } from 'src/app/services/form.service';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.scss']
})
export class AddFormComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fields = []
  types = ['text', 'date', 'email', 'number', 'password', 'select']
  formTypes = ['Normal Form', 'Question']
  userProfile: any
  newMenu: Menu;
  loading: boolean = false
  options: any = []
  currOption: string;
  currOptions: any = []
  constructor(
    private _formBuilder: FormBuilder,
    private menuService: MenuService,
    private authService: AuthService,
    private snackbar: MatSnackBar,
    private formService: FormService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userProfile = this.authService.getUserProfile()
    this.firstFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      type: [''],
      formType: ['', Validators.required],
      canBeShared: [false, Validators.required],
      oneFillOnly: [false, Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      options: [[]],
      type: ['', Validators.required]
    });
    this.thirdFormGroup = this._formBuilder.group({});
  }

  onAddField() {
    if (this.secondFormGroup.valid) {
      this.thirdFormGroup.addControl(this.secondFormGroup.value.name, new FormControl(''))
      this.fields.push({ name: this.secondFormGroup.value.name, type: this.secondFormGroup.value.type, options: this.secondFormGroup.value.options, placeholder: this.secondFormGroup.value.name })
      this.secondFormGroup.reset()
    }
  }

  onAddOption() {
    if (this.currOption != null && this.currOption != '') {
      // this.currOptions.push(this.currOption)
      if (this.secondFormGroup.get("options").value == null) this.secondFormGroup.get("options").setValue([])
      this.secondFormGroup.get("options").value.push(this.currOption)
    }

  }

  onOptionClear(idx) {
    this.secondFormGroup.get("options").value.splice(idx, 1)
  }

  onMenuTypeChanges(event) {
    if (event.value == 'select') {
      this.options = []
      this.options.push('')
    }
  }

  async onSave() {
    this.loading = true
    if (this.firstFormGroup.valid) {
      this.newMenu = this.firstFormGroup.value
      this.newMenu.type = this.newMenu.name
      await this.formService.insertTemplate(this.firstFormGroup.value.type, this.fields)
      if (this.newMenu.canBeShared)
        await this.formService.insertSharedId(this.firstFormGroup.value.type, moment().valueOf())
      this.menuService.add(this.userProfile.key, this.newMenu).then(() => {
        this.snackbarActive("Menu Added", 1500);
        this.router.navigate(['dashboard/homepage'])
      })
      this.loading = false
    } else {
      this.loading = false
    }
  }

  snackbarActive(message: string, duration: number) {
    this.snackbar.open(message, "X", {
      duration: duration,
      verticalPosition: 'top'
    })
  }
}
