import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ProfileService } from 'src/app/services/profile.service';
import { AuthService } from 'src/app/services/auth.service';
import { Profile } from 'src/app/models/profile';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  screenWidth: number;
  fb: FormGroup
  userProfile: Profile;
  loading: boolean = true;
  books = [{ nama: "Bahasa Indonesia", quest: [1, 2, 3] }, { nama: "Matematika", quest: [1, 2, 3] }]
  addquest: boolean;
  questForm: FormGroup;
  userData: any;

  constructor(
    private snackbar: MatSnackBar,
    private profileService: ProfileService,
    private authService: AuthService) {
    this.screenWidth = window.innerWidth
    this.fb = new FormGroup({
      email: new FormControl(null, Validators.required),
      name: new FormControl(null, Validators.required)
    })

    this.questForm = new FormGroup({
      bookName: new FormControl(null, Validators.required),
    })
  }

  ngOnInit() {
    this.userProfile = this.authService.getUserProfile()
    this.profileService.getbyId(this.userProfile.key).subscribe(data => {
      this.userData = data
      this.fb.setValue({ email: data.email, name: data.name || '' })
      this.loading = false
    })
  }

  onAddQuest() {
    this.addquest = !this.addquest
  }

  onUpdate() {
    if (this.fb.valid) {
      let profile = new Profile
      profile = this.fb.value
      profile.key = this.userProfile.key
      this.Update(profile);
    } else {
      this.snackbar.open("Please Fill out all the form to update", "X", {
        duration: 2500,
        verticalPosition: 'top'
      })
    }

  }


  private Update(profile: Profile) {
    this.profileService.update(profile).then(() => {
      this.snackbar.open("Successfully Updated", "X", {
        duration: 1500,
        verticalPosition: 'top'
      });
    })
      .catch(error => {
        if (error) {
          this.snackbar.open("Something Went Wrong, Please try again later", "X", {
            duration: 2500,
            verticalPosition: 'top'
          });
        }
      });
  }
}
