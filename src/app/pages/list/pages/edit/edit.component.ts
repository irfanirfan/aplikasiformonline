import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MenuService } from 'src/app/services/menu.service';
import { AuthService } from 'src/app/services/auth.service';
import { Menu } from 'src/app/models/menu';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormService } from 'src/app/services/form.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  isLinear = false;
  firstFormGroup: FormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    type: [''],
    formType: ['', Validators.required],
    canBeShared: [false, Validators.required],
    oneFillOnly: [false, Validators.required]
  });;
  secondFormGroup: FormGroup = this._formBuilder.group({
    name: ['', Validators.required],
    type: ['', Validators.required]
  });;
  thirdFormGroup: FormGroup;
  fields = []
  types = ['text', 'date', 'email', 'number', 'password']
  formTypes = ['Normal Form', 'Question']
  userProfile: any
  newMenu: Menu;
  loading: boolean = false
  menuKey: string;
  menu: any;
  activeIndex: number;

  constructor(
    private _formBuilder: FormBuilder,
    private menuService: MenuService,
    private authService: AuthService,
    private snackbar: MatSnackBar,
    private formService: FormService,
    private router: Router,
    private activateRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
    this.userProfile = this.authService.getUserProfile()
    this.activateRoute.queryParams.subscribe(res => {
      this.menuKey = res.id
    })
    this.menuService.getById(this.userProfile.key, this.menuKey).subscribe(data => {
      this.menu = data
      this.setFormSettings();
    })

    this.secondFormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      type: ['', Validators.required]
    });

    this.thirdFormGroup = this._formBuilder.group({});
  }

  private setFormSettings() {
    this.firstFormGroup = this._formBuilder.group({
      name: [this.menu.name, Validators.required],
      type: [this.menu.type],
      formType: [this.menu.formType, Validators.required],
      canBeShared: [this.menu.canBeShared, Validators.required],
      oneFillOnly: [this.menu.oneFillOnly, Validators.required]
    });
  }

  onTabChange(event) {
    this.activeIndex = event.index
  }

  onAddField() {
    if (this.secondFormGroup.valid) {
      this.thirdFormGroup.addControl(this.secondFormGroup.value.name, new FormControl(''))
      this.fields.push({ name: this.secondFormGroup.value.name, type: this.secondFormGroup.value.type, placeholder: this.secondFormGroup.value.name })
      this.secondFormGroup.reset()
    }
  }

  async onUpdate() {
    this.loading = true
    if (this.firstFormGroup.valid) {
      this.newMenu = this.firstFormGroup.value
      this.newMenu.type = this.newMenu.name
      await this.setupSharedId();
      await this.updateExistedForm();
      await this.menuService.update(this.userProfile.key, this.menuKey, this.newMenu).then(() => {
        this.snackbarActive("Menu Updated", 1500);
      })
      this.loading = false
    } else {
      this.loading = false
    }
  }

  private async updateExistedForm() {
    if (this.newMenu.name != this.menu.name)
      await this.formService.getForms(this.menu.name).then(async (data) => {
        if (data) {
          await this.formService.insertUpdatedId(this.newMenu.name, data).then(async () => {
            await this.formService.deleteForm(this.menu.name);
          });
        }
      });
  }

  private async setupSharedId() {
    if (this.newMenu.canBeShared)
      await this.formService.insertSharedId(this.firstFormGroup.value.type, moment().valueOf());

    else
      await this.formService.deleteSharedId(this.firstFormGroup.value.name);
  }

  snackbarActive(message: string, duration: number) {
    this.snackbar.open(message, "X", {
      duration: duration,
      verticalPosition: 'top'
    })
  }
}
