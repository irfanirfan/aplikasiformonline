import { Component, OnInit } from '@angular/core';
import { AnimationDurations } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { AuthService } from 'src/app/services/auth.service';
import { Profile } from 'src/app/models/profile';
import { MenuService } from 'src/app/services/menu.service';
import { ShareformComponent } from 'src/app/shared/modal/shareform/shareform.component';
import { ProfileService } from 'src/app/services/profile.service';
import { Router } from '@angular/router';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  displayedColumns = ['name', '']
  dataSource: MatTableDataSource<any>;
  userprofile: Profile;
  menus: any[];
  userData: any;


  constructor(
    private menuService: MenuService,
    private authService: AuthService,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private profileService: ProfileService,
    private popupService: PopupService,
    private route: Router) { }

  ngOnInit() {
    this.userprofile = this.authService.getUserProfile()
    this.profileService.getbyId(this.userprofile.key).subscribe(data => {
      this.userData = data
    })
    this.menuService.get(this.userprofile.key).subscribe(data => {
      this.menus = []
      this.menus = data.map(x => { return { key: x.key, data: x.payload.val() } })
    })
  }

  onShare(menu: any) {
    let dialogRef = this.dialog.open(ShareformComponent, {
      width: '350px'
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        result.forEach(user => {
          // console.log(userKey)
          this.menuService.add(user.key, menu.data).then(() => {
            this.snackbarActive("Shared Succesfully", 1500)
          }).catch(error => {
            this.snackbarActive("Something Went Wrong, please try again later", 2500)
          })
        });
      }
    })
  }

  onEdit(menu: any) {
    let param = { id: menu.key }
    this.route.navigate(['/list/edit'], { queryParams: param })
  }

  snackbarActive(message: string, duration: number) {
    this.snackbar.open(message, "OK", {
      duration: duration,
      verticalPosition: 'top'
    })
  }

  onDelete(menu: any) {
    let param = {
      title: "Delete Form",
      message: "Are you sure want to delete this form?",
      showNoButton: true
    }
    let dialogRef = this.popupService.openDialog(param)
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        return this.menuService.remove(this.userprofile.key, menu.key)
        .then(res=>{this.snackbarActive("Form Deleted Successfully!", 1500)})
          .catch(err => { this.snackbarActive("Failed to remove form, please try again later", 3000) })
      }
    })

  }

}
