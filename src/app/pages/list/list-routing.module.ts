import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list.component';
import { EditComponent } from './pages/edit/edit.component';
import { HomeComponent } from './pages/home/home.component';


let routes: Routes = [
    {
        path: '',
        component: ListComponent,
        children: [
            { path: '', component: HomeComponent, },
            { path: 'edit', component: EditComponent, },
            {
                path: '',
                redirectTo: 'homepage',
                pathMatch: 'full',
            },
        ],
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ListRoutingModule { }
