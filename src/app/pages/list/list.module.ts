import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditComponent } from './pages/edit/edit.component';
import { ListComponent } from './list.component';
import { MaterialModule } from 'src/material/material.module';
import { RouterModule } from '@angular/router';
import { ListRoutingModule } from './list-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HomeComponent } from './pages/home/home.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [EditComponent, ListComponent, HomeComponent],
  imports: [
    CommonModule,
    MaterialModule,
    RouterModule,
    ListRoutingModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ListModule { }
