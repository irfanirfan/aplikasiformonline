import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { FormService } from 'src/app/services/form.service';
import { MenuService } from 'src/app/services/menu.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PopupService } from 'src/app/services/popup.service';
import { AttachmentService } from 'src/app/services/attachment.service';
import { AuthService } from 'src/app/services/auth.service';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-tabledata',
  templateUrl: './tabledata.component.html',
  styleUrls: ['./tabledata.component.scss']
})
export class TabledataComponent implements OnInit {
  forms: any[]
  tableTitle: string
  displayedColumns = []
  dataSource: MatTableDataSource<any> = new MatTableDataSource([])
  type: string;
  userId: string;
  loading: boolean = true
  isOneFillOnly: boolean = false
  isShared: boolean = false
  menu: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  sharedId: any;
  userProfile: Profile;

  constructor(
    private activatedRouted: ActivatedRoute,
    private formService: FormService,
    private router: Router,
    private menuService: MenuService,
    private popupServ: PopupService,
    private attachmentServ: AttachmentService,
    private authServ: AuthService
  ) { }

  ngOnInit() {
    this.getTypeAndUser();
  }

  private getTypeAndUser() {
    this.activatedRouted.paramMap.subscribe(async data => {
      this.type = data.get('type');
      this.userId = data.get('id');
      this.loading = true

      await this.getDisplayColumns();
      this.getMenuTitle();
      this.getDataSource();
    });
  }


  private async getDisplayColumns() {
    await this.formService.getForms(this.type).then((data: any) => {
      this.displayedColumns = [];
      if (data == null)
        return;
      if (data.template.length > 0) {
        data.template.forEach(input => {
          this.displayedColumns.push(input.name);
        });
        this.displayedColumns.push("action");
      }
      if (data.sharedId) {
        this.isShared = true;
        this.sharedId = data.sharedId;
      } else {
        this.isShared = false;
      }
    });
  }

  private getDataSource() {
    let userId = this.userId
    if (this.isShared)
      userId = this.sharedId

      return this.get(userId);

  }

  private get(userId: string) {
    return this.formService.get(userId, this.type).subscribe(data => {
      this.forms = [];
      this.forms = data.map((x: any) => { return { key: x.key, ...x.payload.val() }; });
      this.dataSource = new MatTableDataSource(this.forms);
      this.loading = false;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  getMenuTitle() {
    return this.menuService.getByType(this.userId, this.type).subscribe(data => {
      if (data) {
        this.menu = data.map(x => { return { key: x.key, data: x.payload.val() } })
        if (this.menu.length > 0)
          this.menu = this.menu[0].data
        this.isOneFillOnly = this.menu.oneFillOnly
      }
    })
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onAdd() {
    this.router.navigate([`dashboard/add/`, this.type])
  }

  onEdit(element) {
    let param = { type: this.type, formId: this.userId, id: element.key }
    if (this.isShared)
      param.formId = this.sharedId
    this.router.navigate([`dashboard/edit`], { queryParams: param })
  }

  async onDelete(element) {
    let param = { type: this.type, formId: this.userId, id: element.key }
    if (this.isShared)
      param.formId = this.sharedId

    let dialogRef = this.popupServ.openDialog({ title: "Warning", message: "Are you sure want to delete this data?" })
    dialogRef.afterClosed().subscribe(res => {
      if (res == true)
        this.formService.delete(param)
          .then(() => {
            this.popupServ.successDialog({ title: "Success", message: "Successfully deleted" })
          })
          .catch(err => {
            this.popupServ.errorDialog({ title: "Error", message: err })
          })
    })
  }

  onDownloadReport() {
    this.attachmentServ.downloadExcel(this.dataSource.data, this.type)
  }

}
