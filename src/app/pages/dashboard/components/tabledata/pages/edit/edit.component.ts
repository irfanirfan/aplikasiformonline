import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FormService } from 'src/app/services/form.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Menu } from 'src/app/models/menu';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  inputs: any = []
  fb: FormGroup = new FormGroup({})
  type: string;
  formId: string;
  dataId: string;



  constructor(
    private activateRoute: ActivatedRoute,
    private authService: AuthService,
    private formService: FormService,
    private router: Router) { }

  ngOnInit() {
    this.activateRoute.queryParams.subscribe(async data => {
      this.type = data.type
      this.formId = data.formId
      this.dataId = data.id
      await this.getTemplate();
      this.formService.getById(this.formId, this.type, this.dataId).subscribe(data => {
        this.fb.setValue(data)
      })
    })

  }

  private getTemplate() {
    return this.formService.getTemplate(this.type).then((data: any) => {
      if (data) {
        this.inputs = data;
        if (data.length > 0) {
          data.forEach(input => {
            this.fb.addControl(input.name, new FormControl('', Validators.required));
          });
        }
      }
    });
  }

  onSubmit() {
    if (this.fb.valid) {
      const DATA = this.fb.value
      const param = {
        type: this.type,
        formId: this.formId,
        dataId: this.dataId
      }
      this.formService.update(DATA, param).then(() => {
        this.authService.snackbarActive("Successfully updated", 1500)
        this.router.navigate(['/dashboard/tabledata/' + this.type + '/' + this.authService.getUserProfile().key])
      }).catch(error => {
        this.authService.snackbarActive("Failed to update, because" + error, 3000)
        console.log(error)
      })
    } else {
      this.authService.snackbarActive("Data cannot be empty!", 3000)
    }
  }

}
