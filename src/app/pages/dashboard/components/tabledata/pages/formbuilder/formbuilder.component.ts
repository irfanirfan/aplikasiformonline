import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { FormService } from 'src/app/services/form.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-formbuilder',
  templateUrl: './formbuilder.component.html',
  styleUrls: ['./formbuilder.component.scss']
})
export class FormbuilderComponent implements OnInit, OnDestroy {

  inputs: any = []
  fb: FormGroup = new FormGroup({})
  type: string;
  subcription: Subscription[] = []
  sharedListId: string;
  isShared: boolean = false
  userProfile: Profile;


  constructor(
    private activateRoute: ActivatedRoute,
    private authService: AuthService,
    private formService: FormService,
    private router: Router) { }

  ngOnDestroy(): void {
    this.subcription.forEach(x => x.unsubscribe())
  }

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(data => {
      this.type = data.get('type')
      this.subcription.push(this.getSharedList())
      this.getTemplate()
    })
  }

  private getSharedList() {
    return this.formService.getSharedList(this.type).subscribe(data => {
      if (data) {
        this.isShared = true
        this.sharedListId = data
      } else {
        this.isShared = false
      }
    }, error => {
      this.authService.snackbarActive("Error" + error, 200)
    });
  }

  private getTemplate() {
    return this.formService.getTemplate(this.type).then((data: any) => {
      this.inputs = data;
      if (data.length > 0) {
        data.forEach(input => {
          this.fb.addControl(input.name, new FormControl('', Validators.required));
        });
      }
    }, error => {
      this.authService.snackbarActive("Error" + error, 3000)
    });
  }

  onSubmit() {
    if (this.fb.valid) {
      const DATA = this.fb.value
      let formId: any = this.authService.getUserProfile().key
      if (this.isShared)
        formId = this.sharedListId
      const param = { formId: formId, type: this.type }
      this.formService.insert(DATA, param).then(() => {
        this.authService.snackbarActive("Successfully created", 1500)
        this.router.navigate(['/dashboard/tabledata/' + this.type + '/' + formId])
      }).catch(error => {
        this.authService.snackbarActive("Error" + error, 3000)
      })
    } else {
      this.authService.snackbarActive("Data can't be empty", 3000)
    }
  }

}
