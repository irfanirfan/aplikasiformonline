import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profile';
import { AuthService } from 'src/app/services/auth.service';
import { MemberService } from 'src/app/services/member.service';
import { MenuService } from 'src/app/services/menu.service';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  loading: boolean = true;
  userProfile: Profile;
  userData: any;
  forms:any
  members:any;

  constructor(
    private profileService: ProfileService,
    private memberService: MemberService,
    private menuService:MenuService,
    private authService: AuthService) { }

  ngOnInit() {
    this.userProfile = this.authService.getUserProfile()
    this.getProfileById();
    this.getForms();
    this.getMembers();
  }


  private getMembers() {
    this.memberService.getLimit5().subscribe(data => {
      this.members = data;
    });
  }

  private getForms() {
    this.menuService.getLimit5(this.userProfile.key).subscribe(data => {
      this.forms = data;
    });
  }

  private getProfileById() {
    this.profileService.getbyId(this.userProfile.key).subscribe(data => {
      this.userData = data;
      this.loading = false;
    },
      err => {
        this.authService.snackbarActive("Oops Something went wrong, please try again later", 200);
      });
  }
}
