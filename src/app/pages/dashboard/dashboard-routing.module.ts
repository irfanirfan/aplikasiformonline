import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormbuilderComponent } from './components/tabledata/pages/formbuilder/formbuilder.component';
import { HomepageComponent } from './components/homepage/homepage.component';
import { TabledataComponent } from './components/tabledata/tabledata.component';
import { DashboardComponent } from './dashboard.component';
import { EditComponent } from './components/tabledata/pages/edit/edit.component';


let routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: 'homepage', component: HomepageComponent, },
      { path: 'tabledata/:type/:id', component: TabledataComponent, },
      { path: 'add/:type', component: FormbuilderComponent, },
      { path: 'edit', component: EditComponent, },
      {
        path: '',
        redirectTo: 'homepage',
        pathMatch: 'full',
      },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
