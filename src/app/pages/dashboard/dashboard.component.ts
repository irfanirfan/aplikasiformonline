import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Profile } from 'src/app/models/profile';
import { MenuService } from 'src/app/services/menu.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Menu } from 'src/app/models/menu';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  addMore: boolean = false
  newMenu: Menu = new Menu;
  userProfile: Profile;
  menus: any[];
  loading: boolean = true
  isError: boolean = false
  subcription: Subscription[] = []
  constructor(
    private authService: AuthService,
    private menuService: MenuService,
    private snackbar: MatSnackBar,
    private router: Router) {

  }

  ngOnDestroy(): void {
    this.subcription.forEach(x => x.unsubscribe())
  }

  ngOnInit() {
    this.userProfile = this.authService.getUserProfile()
    if (this.userProfile == null) {
      this.authService.logout()
      return this.router.navigate(['/login']); 
    }
    this.subcription.push(this.getMenu())
  }

  private getMenu() {
    return this.menuService.get(this.userProfile.key).subscribe(data => {
      this.menus = [];
      this.menus = data.map(x => { return { key: x.key, data: x.payload.val() }; });
      this.loading = false;
    }, err => {
      this.loading = false;
      this.snackbarActive("Oops Something went wrong, please try again later", 3000);
    });
  }

  onAddMore() {
    this.addMore = !this.addMore
    if (this.newMenu != null) {
      this.menuService.add(this.userProfile.key, this.newMenu).then(() => {
        this.snackbarActive("Menu Added", 1500);
        this.newMenu = new Menu
      })
    }
  }

  onDelete(menu: any) {
    this.menuService.remove(this.userProfile.key, menu.key).then(() => {
      this.snackbarActive("Menu Removed", 1500)
    }).catch(error => {
      this.loading = false
      this.snackbarActive("Oops Something went wrong, please try again later", 3000)
    })
  }

  snackbarActive(message: string, duration: number) {
    this.snackbar.open(message, "X", {
      duration: duration,
      verticalPosition: 'top'
    })
  }

}
