import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from 'src/material/material.module';
import { HomepageComponent } from './components/homepage/homepage.component';
import { TabledataComponent } from './components/tabledata/tabledata.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EditComponent } from './components/tabledata/pages/edit/edit.component';
import { FormbuilderComponent } from './components/tabledata/pages/formbuilder/formbuilder.component';



@NgModule({
  declarations: [
    HomepageComponent,
    TabledataComponent,
    FormbuilderComponent,
    EditComponent
  ],
  imports: [
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialModule,
    FlexLayoutModule,
  ]
})
export class DashboardModule { }
