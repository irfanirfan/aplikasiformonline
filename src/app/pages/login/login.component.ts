import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignUpComponent } from 'src/app/shared/modal/sign-up/sign-up.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { ProfileService } from 'src/app/services/profile.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  fb: FormGroup
  constructor(
    private router: Router,
    private dialog: MatDialog,
    private snackbar: MatSnackBar,
    private authService: AuthService,
    private profileServ: ProfileService) {
    this.fb = new FormGroup({
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    })
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) { this.router.navigate(['']) }
  }

  onSignIn() {
    if (this.fb.valid) {
      this.authService.login(this.fb.value.email, this.fb.value.password).then(res => {
        this.profileServ.getbyId(res.user.uid).subscribe(data => {
          let dt: any = {
            uid: res.user.uid,
            email: res.user.email,
            level: data.level,
            key: data.key
          }
          localStorage.setItem('userData', JSON.stringify(dt))
          this.snackbar.open('Successfully Login, Welcome Back', "X", {
            duration: 1500,
            verticalPosition: 'top'
          })
          this.router.navigate([''])
        })

      }).catch(error => {
        if (error) {
          this.snackbar.open(error, "X", {
            duration: 2500,
            verticalPosition: 'top'
          })
        }
      })

    } else {
      this.snackbar.open("Your Email Or Password is Invalid", "X", {
        duration: 2500,
        verticalPosition: 'top'
      })
    }
  }

  onSignUp() {
    let dialogRef = this.dialog.open(SignUpComponent, {
      width: '360px'
    })
    dialogRef.afterClosed().subscribe(() => {
      this.router.navigate([''])
    })
  }

  

}
