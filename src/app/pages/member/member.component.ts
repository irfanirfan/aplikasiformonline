import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Profile } from 'src/app/models/profile';
import { MemberService } from 'src/app/services/member.service';
import { PopupService } from 'src/app/services/popup.service';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.scss']
})
export class MemberComponent implements OnInit {
  members: any[];
  userProfile: Profile;
  constructor(
    private authService: AuthService,
    private memberService: MemberService,
    private popupService: PopupService
  ) { }

  ngOnInit() {
    this.userProfile = this.authService.getUserProfile()
    this.memberService.getAll().subscribe(data => {
      this.members = []
      this.members = data.map(x => { return { key: x.key, data: x.payload.val() } })
    })
  }

  onDelete(member) {
    const param = {
      showNoButton: true,
      showOkButton: true,
      message: "Are you sure want to delete this member?",
      title: "Delete Member"
    }
    let dialogRef = this.popupService.openDialog(param)
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.authService.remove(member.data.email, member.data.password)
        this.memberService.remove(member.key).then(x => {
          return this.authService.snackbarActive("User Deleted Successfully!", 1500)
        }).catch(err => {
          return this.popupService.errorDialog("Something went wrong, please try again later")
        });
      }
    })

  }

}
