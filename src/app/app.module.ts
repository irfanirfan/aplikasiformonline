import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material/material.module'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire'
import { AngularFireAuthModule } from '@angular/fire/auth'
import { AngularFireDatabaseModule } from '@angular/fire/database'
import * as firebaseConfig from '../environments/firebaseConfig'
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './pages/login/login.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SignUpComponent } from './shared/modal/sign-up/sign-up.component';
import { AuthService } from './services/auth.service';
import { AuthguardGuard } from './services/authguard.guard';
import { ProfileService } from './services/profile.service';
import { AddMenuModalComponent } from './shared/add-menu-modal/add-menu-modal.component';
import { FDKComponent } from './shared/form/fdk/fdk.component';
import { SIPKComponent } from './shared/form/sipk/sipk.component';
import { SPJDComponent } from './shared/form/spjd/spjd.component';
import { PITComponent } from './shared/form/pit/pit.component';
import { FormService } from './services/form.service';
import { AddFormComponent } from './pages/add-form/add-form.component';
import { ShareformComponent } from './shared/modal/shareform/shareform.component';
import { MemberComponent } from './pages/member/member.component';
import { MemberService } from './services/member.service';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { NgChartsModule } from 'ng2-charts';
import { DialogComponent } from './shared/modal/dialog/dialog.component';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
    SignUpComponent,
    AddMenuModalComponent,
    FDKComponent,
    SIPKComponent,
    SPJDComponent,
    PITComponent,
    AddFormComponent,
    ShareformComponent,
    MemberComponent,
    DialogComponent
  ],
  imports: [
    FormsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig.FirebaseConfig),
    AngularFireAnalyticsModule,
    NgChartsModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,

  ],
  entryComponents: [
    ShareformComponent,
    SignUpComponent
  ]
  ,
  providers: [AuthService, AuthguardGuard, ProfileService, FormService, MemberService, { provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
